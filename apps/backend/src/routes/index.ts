import { FastifyInstance } from "fastify";
import { askAI } from "../core";

export async function appRoutes(app: FastifyInstance) {
  app.get("/", () => {
    return {
      message: "Hello World!",
    };
  });

  app.post("/chat", async (request, reply) => {
    // check if body is json and parse
    let bodyParsed = null;
    if (typeof request.body === "string") {
      try {
        bodyParsed = JSON.parse(request.body);
      } catch (error) {
        return reply.status(400).send({
          message: "Invalid JSON body",
        });
      }
    }
    const response = await askAI(bodyParsed);
    return { response };
  });
}
