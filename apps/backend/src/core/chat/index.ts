import { AIMessage, HumanMessage } from "@langchain/core/messages";
import { StructuredOutputParser } from "@langchain/core/output_parsers";
import { ChatPromptTemplate } from "@langchain/core/prompts";
import { RunnableSequence } from "@langchain/core/runnables";
import { ChatMistralAI } from "@langchain/mistralai";
import { z } from "zod";

type ChatInput = {
  messages: { role: "user" | "assistant"; content: string }[];
  question: string;
};

const zodSchema = z.object({
  answer: z.string().describe("answer to the user's question"),
});

export async function askAI(input: unknown): Promise<unknown> {
  const chatInputs = input as ChatInput;

  const model = new ChatMistralAI({
    model: "ft:open-mistral-7b:54042f93:20240613:1fee966a",
    temperature: 0,
  });

  const parser = StructuredOutputParser.fromZodSchema(zodSchema);

  const chatInputModel = chatInputs.messages.map((input) =>
    input.role === "user"
      ? new HumanMessage(input.content)
      : new AIMessage(input.content)
  );

  const chain = RunnableSequence.from([
    ChatPromptTemplate.fromMessages([
      [
        "system",
        "Answer the users question as best as possible.\n{format_instructions}\n{question}",
      ],
      ...chatInputModel,
      new HumanMessage(chatInputs.question),
    ]),
    model,
    parser,
  ]);

  const response = await chain.invoke({
    question: chatInputs.question,
    format_instructions: parser.getFormatInstructions(),
  });

  return response;
}
