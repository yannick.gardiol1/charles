// server.ts
import cors from "@fastify/cors";
import * as dotenv from "dotenv";
import fastify from "fastify";
import { AddressInfo } from "net";
import { appRoutes } from "./routes";

export function initServer() {
  const server = fastify({ logger: true });

  server.register(cors, {
    origin: "*",
    methods: ["GET", "POST"],
  });
  server.register(appRoutes);

  return server;
}

const start = async () => {
  // Execute the app when called directly( ex.: "npm run dev")
  if (require.main === module) {
    dotenv.config();
    const server = initServer();
    try {
      await server.listen({ port: 3000 });
      server.log.info(
        `server listening on ${(server.server.address() as AddressInfo)?.port}`
      );
    } catch (err) {
      server.log.error(err);
      process.exit(1);
    }
  }
};

start();
