import os
import pandas as pd

# Chemins d'accès
pathInputDirectory = 'input'
pathOutputDirectory = 'output'
pathInput = f'{pathInputDirectory}/dataset_mental_health_stephane.csv'
pathOutput = f'{pathOutputDirectory}/dataset_mental_health_stephane.parquet'

# Créer un DataFrame exemple
df = pd.read_csv(pathInput)

os.makedirs(pathOutputDirectory, exist_ok=True)

df.to_parquet(pathOutput, engine='pyarrow', compression='snappy')

# Relire le fichier Parquet pour vérifier
df_parquet = pd.read_parquet(pathOutput)
print(df_parquet)