# Training

## Configuration

Creating a virtual environment in Python

```bash
python3 -m venv env
```

Install Poetry [Doc](https://python-poetry.org/docs/#installing-manually)

```bash
env/bin/pip install -U pip setuptools
env/bin/pip install poetry
```

Add Interpreter

- Execute command for copy path in clipboard

```bash
env/bin/poetry env info --path | pbcopy
```

- Cmd + Shift + P > Python: Select Interpreter
- Click on `Enter interpreter path...`
- Paster the path
- Press Enter
