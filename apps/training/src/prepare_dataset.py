

import os
import pandas as pd

def prepare_dataset(url: str, file_name: str) -> tuple[str, str]:
    """
    Reads a parquet file from a URL, splits the data into training and evaluation sets,
    and saves them as JSONL files.

    Returns:
        None
    """
    df = pd.read_parquet(url)

    df_train = df.sample(frac=0.995, random_state=200)
    df_eval = df.drop(df_train.index)

    # Create the dataset directory if it doesn't exist
    os.makedirs("dataset", exist_ok=True)

    train_file_path = f"dataset/{file_name}_train.jsonl"
    eval_file_path = f"dataset/{file_name}_eval.jsonl"
    
    df_train.to_json(train_file_path, orient="records", lines=True)
    df_eval.to_json(eval_file_path.format(file_name), orient="records", lines=True)

    return train_file_path, eval_file_path



