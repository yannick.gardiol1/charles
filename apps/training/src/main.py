from create_jobs import create_job
from libs.create_mistral_client import create_mistral_client
from libs.transform_to_json import transform_to_json
from libs.upload_dataset import upload_dataset
from prepare_dataset import prepare_dataset
from reformat_dataset import reformat_dataset


url = 'hf://datasets/vivekdugale/llama2_chat_mental_health_convo_amod_3.51k/data/train-00000-of-00001.parquet'
base_file_name = 'llama2_chat_mental_health_convo_amod_3.51k'

print("Preparing dataset")
file_name_train, file_name_eval = prepare_dataset(url, base_file_name)
transform_to_json(file_name_train)
transform_to_json(file_name_eval)
#reformat_dataset(file_name_train, file_name_eval)

client = create_mistral_client()

dataset_chunk_train = upload_dataset(client, file_name_train, f"{base_file_name}_train.jsonl")
dateset_chunk_eval = upload_dataset(client, file_name_eval, f"{base_file_name}_eval.jsonl")

job_id = create_job(client, "open-mistral-7b", dataset_chunk_train, dateset_chunk_eval)

print(f"Job created with id: {job_id}")