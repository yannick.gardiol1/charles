from mistralai.models.chat_completion import ChatMessage
from libs.create_mistral_client import create_mistral_client

client = create_mistral_client()

retrieved_job = client.jobs.retrieve("1fee966a-f003-4cbb-80cb-eaa29d0c4240")

chat_response = client.chat(
    model=retrieved_job.fine_tuned_model,
    messages=[ChatMessage(role='user', content='What is the best French cheese?')]
)

print(chat_response)