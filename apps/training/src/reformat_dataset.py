from libs.reformat_data import reformat_jsonl

def reformat_dataset(file_path_train: str, file_path_eval: str):
    reformat_jsonl(file_path_train)
    reformat_jsonl(file_path_eval)