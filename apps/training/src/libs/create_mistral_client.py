#!/usr/bin/env python3
import os
from dotenv import load_dotenv
from mistralai.client import MistralClient

load_dotenv()

def create_mistral_client():
    """
    Create a Mistral client
    :return: MistralClient - client to interact with Mistral
    """
    api_key = os.environ.get("MISTRAL_API_KEY")
    client = MistralClient(api_key=api_key)
    return client