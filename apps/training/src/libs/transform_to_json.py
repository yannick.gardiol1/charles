
import os
import re
import json


def transform_to_json(input_file):
    output_file = input_file + ".tmp"
    with open(input_file, "r") as infile, open(output_file, "w") as outfile:
        for idx, line in enumerate(infile):
            data = json.loads(line)
            text = data['text']

            # Supprimer les balises <s> et </s>
            clean_data = re.sub(r"<\/?s>", "", text)
            
            # Séparer les interactions
            user_text, assistant_text = re.split(r"\[\/INST\]", clean_data)
            user_text = user_text.replace("[INST]", "").strip()
            assistant_text = assistant_text.strip()

            # ignore les lignes ou assistant_text est vide
            if not assistant_text:
                continue
            
            # Construire le format JSON
            interactions = [
                {"role": "user", "content": user_text},
                {"role": "assistant", "content": assistant_text}
            ]
            
            json_data = {
                "messages": interactions
            }
            outfile.write(json.dumps(json_data) + "\n")
    
    os.rename(output_file, input_file)
