#!/usr/bin/env python3
from mistralai.client import MistralClient
from mistralai.models.files import FileObject


def upload_dataset(client: MistralClient, file_path: str, file_name: str) -> FileObject:
    """
    Upload a dataset to Mistral
    :param client: MistralClient - client to interact with Mistral
    :param file_path: str - path to the file to upload
    :param file_name: str - name of the file
    :return: FileObject - uploaded file
    """
    with open(file_path, "rb") as f:
        file = client.files.create(file=(file_name, f))
    
    return file