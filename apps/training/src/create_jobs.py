from mistralai.models.jobs import TrainingParameters
from libs.create_mistral_client import create_mistral_client
from libs.upload_dataset import upload_dataset
from mistralai.client import MistralClient
from mistralai.models.files import FileObject

def create_job(client: MistralClient, model: str, dataset_chunk_train: FileObject, dataset_chunk_eval: FileObject):
    """
    Create a job with the given training and evaluation datasets
    :param client: MistralClient - client to interact with Mistral
    :param model: str - model name
    :param dataset_chunk_train: FileObject - training dataset
    :param dataset_chunk_eval: FileObject - evaluation dataset
    :return: str - job id
    """

    print(f'Creating job for model {model} with training dataset {dataset_chunk_train.id} and evaluation dataset {dataset_chunk_eval.id}')
    created_jobs = client.jobs.create(
        model=model,
        training_files=[dataset_chunk_train.id],
        validation_files=[dataset_chunk_eval.id],
        hyperparameters=TrainingParameters(
            training_steps=10,
            learning_rate=0.0001,
            )
    )
    return created_jobs.id