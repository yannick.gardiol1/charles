import Charles from "../../assets/Charles.png";
import Dictaphone from "./components/Dictaphone";
import { useChatState } from "./state";

const Chat = () => {
  const messages = useChatState((state) => state.messages);

  return (
    <div className="flex flex-col justify-between h-full flex-1">
      <div className="mt-5 flex justify-center gap-3 items-center">
        <div className="flex flex-col items-center">
          <p className="text-center">Hello, I'm Charles your personnal <br /> AI compagnon.</p>
          <p>How can I help you ?</p>
        </div>
        <img src={Charles} alt="Charles" className="h-40 w-40" />
      </div>
      <Dictaphone />
    </div>
  );
};

export default Chat;
