import SpeechRecognition, {
  useSpeechRecognition,
} from "react-speech-recognition";
import microphone from "../../../assets/microphone.svg";
import { useChatState } from "../state";

const Dictaphone = () => {
  const { addMessage, messages } = useChatState((state) => ({ addMessage: state.addMessage, messages: state.messages}));

  const {
    transcript,
    listening,
    resetTranscript,
    browserSupportsSpeechRecognition,
  } = useSpeechRecognition();

  const handleMouseClick = () => {
    if (listening) {
      handleMouseUp();
    } else {
      handleMouseDown();
    }
  }

  const handleMouseDown = () => {
    resetTranscript();
    SpeechRecognition.startListening({ continuous: true});
  };

  const handleMouseUp = async () => {
    await SpeechRecognition.stopListening();

    if(!transcript) return;

    addMessage({ text: transcript, author: "user", date: new Date() });

    const response = await fetch("http://localhost:3000/chat", {
      method: "POST",
      body: JSON.stringify({
        messages: messages.map((message) => { return { content: message.text, role: message.author }; }),
        question: transcript
      })
    });
    const responseAI = await response.json();

    
    addMessage({ text: responseAI.response.answer, author: "assistant", date: new Date() });

    resetTranscript();
  };

  if (!browserSupportsSpeechRecognition) {
    return <span>Browser doesn't support speech recognition.</span>;
  }

  return (
    <div className="bg-dark flex flex-col items-center space-y-4">
      <button
        className="flex items-center justify-center w-16 h-16 bg-grey-500 text-white rounded-full shadow-lg hover:bg-blue-500 focus:outline-none"
        onClick={handleMouseClick}
      >
        <img src={microphone} alt="Microphone" className="h-8 w-8" />
      </button>
      <p>Record: {listening ? "on" : "off"}</p>
      <p>{transcript}</p>
    </div>
  );
};

export default Dictaphone;
