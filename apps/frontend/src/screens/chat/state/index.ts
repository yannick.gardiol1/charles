import { create } from "zustand";

type Message = { text: string; author: "user" | "assistant"; date: Date };

type State = {
  messages: Message[];
};

type Actions = {
  addMessage: (message: Message) => void;
};

export const useChatState = create<State & Actions>((set) => ({
  messages: [],
  addMessage: (message) =>
    set((state) => ({ messages: [...state.messages, message] })),
}));
