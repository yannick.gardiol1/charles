import 'regenerator-runtime/runtime';
import "./App.css";
import Chat from "./screens/chat";
import TopBar from "./ui/TopBar";

function App() {

  return (
    <>
      <div className="min-h-screen flex flex-col">
        <TopBar />
        <Chat />
      </div>
    </>
  );
}

export default App;
