import React from "react";

const TopBar: React.FC = () => {
  return (
    <div className="bg-white backdrop-blur-sm w-full py-4 px-4 shadow-md z-10">
      <div className="container mx-auto flex justify-between items-center">
        <div className="hidden md:block">
          <span className="text-2xl font-bold">Charles</span>
        </div>
        <div className="md:hidden flex-1 text-center">
          <span className="text-2xl font-bold">Charles</span>
        </div>
        <div className="md:flex-1 hidden md:block"></div>
      </div>
    </div>
  );
};

export default TopBar;
