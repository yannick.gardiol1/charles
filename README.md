# Charles

## Presentation

The project’s goal is to create a personalized assistant capable of helping isolated individuals by providing them with psychological support, as well as enabling them to establish social connections.

## Apps

### training

Application for fine-tuning Mistral’s LLM.

Also includes the preparation of the dataset in the format expected by Mistral

### create_dataset

Application to create a dataset in Parquet format from a CSV source.

### backend

Backend application exposing an API to query the fine-tuned LLM.

Uses Lanchain with Langsmith to monitor calls and better evaluate prompt evolutions.

### frontend

Web application that allows speaking to the assistant using the Web Speech API to reduce initial costs and improve performance.
